# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2018-2020 Harald Sitter <sitter@kde.org>

all: geoip-kde-org doc

clean:
	rm -rf doc geoip-kde-org

node_modules/.bin/apidoc:
	npm install --cache .npm apidoc

GeoLite2-City.mmdb:
	scp geoip@hepta.kde.org:/home/geoip/data/GeoLite2-City.mmdb .

deploy: geoip-kde-org test
	./deploy

doc: node_modules/.bin/apidoc
	node_modules/.bin/apidoc \
		--debug \
		-e node_modules \
		-e vendor \
		-e doc \
		-e /\.go \
		-e /\.npm \
		-e /\.git \
		-e /\.ruby \
		-o doc

install:
	systemctl --user stop geoip-kde-org.service || true
	go install
	cp -rv systemd/* ~/.config/systemd/user/
	systemctl --user daemon-reload
	systemctl --user enable geoip-kde-org.socket
	systemctl --user restart geoip-kde-org.socket

test: GeoLite2-City.mmdb
	go test -v ./...

geoip-kde-org:
	go build -o geoip-kde-org -v

run: geoip-kde-org
	/usr/bin/systemd-socket-activate -l 0.0.0.0:8080 ./geoip-kde-org

.PHONY: doc deploy test run
