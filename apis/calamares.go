// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: 2018 Harald Sitter <sitter@kde.org>

package apis

import (
	"net/http"

	"github.com/gin-gonic/gin"
	geoip2 "github.com/oschwald/geoip2-golang"
	"invent.kde.org/sysadmin/geoip-service-backend.git/models"
)

// We are muddying the waters a bit by merging api+service+data.
type calamaresResource struct {
	db *geoip2.Reader
}

// ServeCalamaresResource sets up the calamares resource routes.
func ServeCalamaresResource(rg *gin.RouterGroup, db *geoip2.Reader) {
	r := &calamaresResource{db}
	rg.GET("/v1/calamares", r.get)
}

/**
 * @api {get} /calamares Calamares
 *
 * @apiVersion 1.0.0
 * @apiGroup GeoIP
 * @apiName calamares
 *
 * @apiDescription Calamares-style JSON geoip data. This endpont offers the
 *   JSON format defined by Calamares' locale module.
 *
 * @apiSuccessExample {json} Success-Response:
 *   {"time_zone":"Europe/Vienna"}
 */
func (r *calamaresResource) get(c *gin.Context) {
	// If you are using strings that may be invalid, check that ip is not nil
	record, err := r.db.City(clientIP(c))
	if err != nil {
		panic(err)
	}

	data := models.CalamaresGeoIP{TimeZone: record.Location.TimeZone}
	c.JSON(http.StatusOK, data)
}
