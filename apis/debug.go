// SPDX-License-Identifier: AGPL-3.0-or-later
// SPDX-FileCopyrightText: 2018 Harald Sitter <sitter@kde.org>

package apis

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	geoip2 "github.com/oschwald/geoip2-golang"
)

type debugResource struct {
	db *geoip2.Reader
}

// ServeDebugResource sets up the semi-internal data inspection resource.
// Its format is entirely undefined and absolutely not meant to for consumption.
func ServeDebugResource(rg *gin.RouterGroup, db *geoip2.Reader) {
	r := &debugResource{db}
	rg.GET("/debug", r.get)
}

func (r *debugResource) get(c *gin.Context) {
	// If you are using strings that may be invalid, check that ip is not nil
	record, err := r.db.City(clientIP(c))
	if err != nil {
		panic(err)
	}
	fmt.Printf("%+v\n", record)

	c.JSON(http.StatusOK, record)
}
