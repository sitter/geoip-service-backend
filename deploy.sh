#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2018-2020 Harald Sitter <sitter@kde.org>

set -ex

name='geoip-kde-org'
ssh='ssh geoip@hepta.kde.org --'
scp_target='geoip@hepta.kde.org'
home='/home/geoip'
bin="${home}/bin"
data="${home}/data"
systemd="${home}/.config/systemd/user"

$ssh mkdir -p $bin
$ssh mkdir -p $data
$ssh mkdir -p $systemd

rsync -avz --progress -e ssh $name $scp_target:$bin
rsync -avz --progress -e ssh systemd/ $scp_target:$systemd
rsync -avz --progress -e ssh doc $scp_target:$home/data/

$ssh systemctl --user daemon-reload
$ssh systemctl --user enable geoip-kde-org.socket
# Sockets cannot be stopped while the service is running. Since we'll want to
# have zero downtime we do not force restart the socket but instead stop the
# service. What happens then is that our service refuses new connections but
# still works off pending requests. Once it is down or a timeout is hit the
# service actually stops. Meanwhile the socket queues new requests. Once
# the service is stopped the socket starts a new instance of the service and
# feeds it the queued requests. Thereby we can have zero downtime so long
# as the socket itself doesn't need restarting.
$ssh systemctl --user start geoip-kde-org.socket || true
$ssh systemctl --user stop geoip-kde-org.service || true
